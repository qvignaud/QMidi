# QMidi include file for QMake
CONFIG += c++11

INCLUDEPATH += $$PWD
SOURCES += \
	$$PWD/QMidiFile.cpp \
    $$PWD/QMidiNote.cpp

HEADERS += \
	$$PWD/QMidiFile.h \
    $$PWD/QMidiNote.h

!mac {
    SOURCES += $$PWD/QMidiOut.cpp
    HEADERS += $$PWD/QMidiOut.h
}

win32 {
	LIBS += -lwinmm
	SOURCES += $$PWD/OS/QMidi_Win32.cpp
}

linux* {
	LIBS += -lasound
	SOURCES += $$PWD/OS/QMidi_ALSA.cpp
}

haiku* {
	LIBS += -lmidi2
	SOURCES += $$PWD/OS/QMidi_Haiku.cpp
}
