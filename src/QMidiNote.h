/*
 * Copyright 2018-2019 Quentin Vignaud
 * All rights reserved. Distributed under the terms of the MIT license.
 */

#ifndef QMIDINOTE_H
#define QMIDINOTE_H

#include <QLocale>
#include <QException>

class QMidiNoteException : public QException
{
public:
    enum Reason {
        OutOfBounds,
        UnknownNote
    };

    QMidiNoteException(Reason reason, int note);

    const Reason reason;
    const int note;

    QMidiNoteException *clone() const;
    void raise() const;
};


class QMidiNote
{
public:
    enum Note : int {
        // MIDI note 0
        // Do Re Mi Fa Sol La Si
        Do_1	=	0x0,
        Sid_1	=	0x0,
        SiM_1	=	0x0,
        // D E F G A B C
        C_1	=	0x0,
        Bd_1	=	0x0,
        BM_1	=	0x0,

        // MIDI note 1
        // Do Re Mi Fa Sol La Si
        Dod_1	=	0x1,
        DoM_1	=	0x1,
        Reb_1	=	0x1,
        Rem_1	=	0x1,
        // D E F G A B C
        Cd_1	=	0x1,
        CM_1	=	0x1,
        Db_1	=	0x1,
        Dm_1	=	0x1,

        // MIDI note 2
        // Do Re Mi Fa Sol La Si
        Re_1	=	0x2,
        // D E F G A B C
        D_1	=	0x2,

        // MIDI note 3
        // Do Re Mi Fa Sol La Si
        Red_1	=	0x3,
        ReM_1	=	0x3,
        Mib_1	=	0x3,
        Mim_1	=	0x3,
        // D E F G A B C
        Dd_1	=	0x3,
        DM_1	=	0x3,
        Eb_1	=	0x3,
        Em_1	=	0x3,

        // MIDI note 4
        // Do Re Mi Fa Sol La Si
        Mi_1	=	0x4,
        Fab_1	=	0x4,
        Fam_1	=	0x4,
        // D E F G A B C
        E_1	=	0x4,
        Fb_1	=	0x4,
        Fm_1	=	0x4,

        // MIDI note 5
        // Do Re Mi Fa Sol La Si
        Fa_1	=	0x5,
        Mid_1	=	0x5,
        MiM_1	=	0x5,
        // D E F G A B C
        F_1	=	0x5,
        Ed_1	=	0x5,
        EM_1	=	0x5,

        // MIDI note 6
        // Do Re Mi Fa Sol La Si
        Fad_1	=	0x6,
        FaM_1	=	0x6,
        Solb_1	=	0x6,
        Solm_1	=	0x6,
        // D E F G A B C
        Fd_1	=	0x6,
        FM_1	=	0x6,
        Gb_1	=	0x6,
        Gm_1	=	0x6,

        // MIDI note 7
        // Do Re Mi Fa Sol La Si
        Sol_1	=	0x7,
        // D E F G A B C
        G_1	=	0x7,

        // MIDI note 8
        // Do Re Mi Fa Sol La Si
        Sold_1	=	0x8,
        SolM_1	=	0x8,
        Lab_1	=	0x8,
        Lam_1	=	0x8,
        // D E F G A B C
        Gd_1	=	0x8,
        GM_1	=	0x8,
        Ab_1	=	0x8,
        Am_1	=	0x8,

        // MIDI note 9
        // Do Re Mi Fa Sol La Si
        La_1	=	0x9,
        // D E F G A B C
        A_1	=	0x9,

        // MIDI note 10
        // Do Re Mi Fa Sol La Si
        Lad_1	=	0xa,
        LaM_1	=	0xa,
        Sib_1	=	0xa,
        Sim_1	=	0xa,
        // D E F G A B C
        Ad_1	=	0xa,
        AM_1	=	0xa,
        Bb_1	=	0xa,
        Bm_1	=	0xa,

        // MIDI note 11
        // Do Re Mi Fa Sol La Si
        Si_1	=	0xb,
        Dob_1	=	0xb,
        Dom_1	=	0xb,
        // D E F G A B C
        B_1	=	0xb,
        Cb_1	=	0xb,
        Cm_1	=	0xb,

        // MIDI note 12
        // Do Re Mi Fa Sol La Si
        Do0	=	0xc,
        Sid0	=	0xc,
        SiM0	=	0xc,
        // D E F G A B C
        C0	=	0xc,
        Bd0	=	0xc,
        BM0	=	0xc,

        // MIDI note 13
        // Do Re Mi Fa Sol La Si
        Dod0	=	0xd,
        DoM0	=	0xd,
        Reb0	=	0xd,
        Rem0	=	0xd,
        // D E F G A B C
        Cd0	=	0xd,
        CM0	=	0xd,
        Db0	=	0xd,
        Dm0	=	0xd,

        // MIDI note 14
        // Do Re Mi Fa Sol La Si
        Re0	=	0xe,
        // D E F G A B C
        D0	=	0xe,

        // MIDI note 15
        // Do Re Mi Fa Sol La Si
        Red0	=	0xf,
        ReM0	=	0xf,
        Mib0	=	0xf,
        Mim0	=	0xf,
        // D E F G A B C
        Dd0	=	0xf,
        DM0	=	0xf,
        Eb0	=	0xf,
        Em0	=	0xf,

        // MIDI note 16
        // Do Re Mi Fa Sol La Si
        Mi0	=	0x10,
        Fab0	=	0x10,
        Fam0	=	0x10,
        // D E F G A B C
        E0	=	0x10,
        Fb0	=	0x10,
        Fm0	=	0x10,

        // MIDI note 17
        // Do Re Mi Fa Sol La Si
        Fa0	=	0x11,
        Mid0	=	0x11,
        MiM0	=	0x11,
        // D E F G A B C
        F0	=	0x11,
        Ed0	=	0x11,
        EM0	=	0x11,

        // MIDI note 18
        // Do Re Mi Fa Sol La Si
        Fad0	=	0x12,
        FaM0	=	0x12,
        Solb0	=	0x12,
        Solm0	=	0x12,
        // D E F G A B C
        Fd0	=	0x12,
        FM0	=	0x12,
        Gb0	=	0x12,
        Gm0	=	0x12,

        // MIDI note 19
        // Do Re Mi Fa Sol La Si
        Sol0	=	0x13,
        // D E F G A B C
        G0	=	0x13,

        // MIDI note 20
        // Do Re Mi Fa Sol La Si
        Sold0	=	0x14,
        SolM0	=	0x14,
        Lab0	=	0x14,
        Lam0	=	0x14,
        // D E F G A B C
        Gd0	=	0x14,
        GM0	=	0x14,
        Ab0	=	0x14,
        Am0	=	0x14,

        // MIDI note 21
        // Do Re Mi Fa Sol La Si
        La0	=	0x15,
        // D E F G A B C
        A0	=	0x15,

        // MIDI note 22
        // Do Re Mi Fa Sol La Si
        Lad0	=	0x16,
        LaM0	=	0x16,
        Sib0	=	0x16,
        Sim0	=	0x16,
        // D E F G A B C
        Ad0	=	0x16,
        AM0	=	0x16,
        Bb0	=	0x16,
        Bm0	=	0x16,

        // MIDI note 23
        // Do Re Mi Fa Sol La Si
        Si0	=	0x17,
        Dob0	=	0x17,
        Dom0	=	0x17,
        // D E F G A B C
        B0	=	0x17,
        Cb0	=	0x17,
        Cm0	=	0x17,

        // MIDI note 24
        // Do Re Mi Fa Sol La Si
        Do1	=	0x18,
        Sid1	=	0x18,
        SiM1	=	0x18,
        // D E F G A B C
        C1	=	0x18,
        Bd1	=	0x18,
        BM1	=	0x18,

        // MIDI note 25
        // Do Re Mi Fa Sol La Si
        Dod1	=	0x19,
        DoM1	=	0x19,
        Reb1	=	0x19,
        Rem1	=	0x19,
        // D E F G A B C
        Cd1	=	0x19,
        CM1	=	0x19,
        Db1	=	0x19,
        Dm1	=	0x19,

        // MIDI note 26
        // Do Re Mi Fa Sol La Si
        Re1	=	0x1a,
        // D E F G A B C
        D1	=	0x1a,

        // MIDI note 27
        // Do Re Mi Fa Sol La Si
        Red1	=	0x1b,
        ReM1	=	0x1b,
        Mib1	=	0x1b,
        Mim1	=	0x1b,
        // D E F G A B C
        Dd1	=	0x1b,
        DM1	=	0x1b,
        Eb1	=	0x1b,
        Em1	=	0x1b,

        // MIDI note 28
        // Do Re Mi Fa Sol La Si
        Mi1	=	0x1c,
        Fab1	=	0x1c,
        Fam1	=	0x1c,
        // D E F G A B C
        E1	=	0x1c,
        Fb1	=	0x1c,
        Fm1	=	0x1c,

        // MIDI note 29
        // Do Re Mi Fa Sol La Si
        Fa1	=	0x1d,
        Mid1	=	0x1d,
        MiM1	=	0x1d,
        // D E F G A B C
        F1	=	0x1d,
        Ed1	=	0x1d,
        EM1	=	0x1d,

        // MIDI note 30
        // Do Re Mi Fa Sol La Si
        Fad1	=	0x1e,
        FaM1	=	0x1e,
        Solb1	=	0x1e,
        Solm1	=	0x1e,
        // D E F G A B C
        Fd1	=	0x1e,
        FM1	=	0x1e,
        Gb1	=	0x1e,
        Gm1	=	0x1e,

        // MIDI note 31
        // Do Re Mi Fa Sol La Si
        Sol1	=	0x1f,
        // D E F G A B C
        G1	=	0x1f,

        // MIDI note 32
        // Do Re Mi Fa Sol La Si
        Sold1	=	0x20,
        SolM1	=	0x20,
        Lab1	=	0x20,
        Lam1	=	0x20,
        // D E F G A B C
        Gd1	=	0x20,
        GM1	=	0x20,
        Ab1	=	0x20,
        Am1	=	0x20,

        // MIDI note 33
        // Do Re Mi Fa Sol La Si
        La1	=	0x21,
        // D E F G A B C
        A1	=	0x21,

        // MIDI note 34
        // Do Re Mi Fa Sol La Si
        Lad1	=	0x22,
        LaM1	=	0x22,
        Sib1	=	0x22,
        Sim1	=	0x22,
        // D E F G A B C
        Ad1	=	0x22,
        AM1	=	0x22,
        Bb1	=	0x22,
        Bm1	=	0x22,

        // MIDI note 35
        // Do Re Mi Fa Sol La Si
        Si1	=	0x23,
        Dob1	=	0x23,
        Dom1	=	0x23,
        // D E F G A B C
        B1	=	0x23,
        Cb1	=	0x23,
        Cm1	=	0x23,

        // MIDI note 36
        // Do Re Mi Fa Sol La Si
        Do2	=	0x24,
        Sid2	=	0x24,
        SiM2	=	0x24,
        // D E F G A B C
        C2	=	0x24,
        Bd2	=	0x24,
        BM2	=	0x24,

        // MIDI note 37
        // Do Re Mi Fa Sol La Si
        Dod2	=	0x25,
        DoM2	=	0x25,
        Reb2	=	0x25,
        Rem2	=	0x25,
        // D E F G A B C
        Cd2	=	0x25,
        CM2	=	0x25,
        Db2	=	0x25,
        Dm2	=	0x25,

        // MIDI note 38
        // Do Re Mi Fa Sol La Si
        Re2	=	0x26,
        // D E F G A B C
        D2	=	0x26,

        // MIDI note 39
        // Do Re Mi Fa Sol La Si
        Red2	=	0x27,
        ReM2	=	0x27,
        Mib2	=	0x27,
        Mim2	=	0x27,
        // D E F G A B C
        Dd2	=	0x27,
        DM2	=	0x27,
        Eb2	=	0x27,
        Em2	=	0x27,

        // MIDI note 40
        // Do Re Mi Fa Sol La Si
        Mi2	=	0x28,
        Fab2	=	0x28,
        Fam2	=	0x28,
        // D E F G A B C
        E2	=	0x28,
        Fb2	=	0x28,
        Fm2	=	0x28,

        // MIDI note 41
        // Do Re Mi Fa Sol La Si
        Fa2	=	0x29,
        Mid2	=	0x29,
        MiM2	=	0x29,
        // D E F G A B C
        F2	=	0x29,
        Ed2	=	0x29,
        EM2	=	0x29,

        // MIDI note 42
        // Do Re Mi Fa Sol La Si
        Fad2	=	0x2a,
        FaM2	=	0x2a,
        Solb2	=	0x2a,
        Solm2	=	0x2a,
        // D E F G A B C
        Fd2	=	0x2a,
        FM2	=	0x2a,
        Gb2	=	0x2a,
        Gm2	=	0x2a,

        // MIDI note 43
        // Do Re Mi Fa Sol La Si
        Sol2	=	0x2b,
        // D E F G A B C
        G2	=	0x2b,

        // MIDI note 44
        // Do Re Mi Fa Sol La Si
        Sold2	=	0x2c,
        SolM2	=	0x2c,
        Lab2	=	0x2c,
        Lam2	=	0x2c,
        // D E F G A B C
        Gd2	=	0x2c,
        GM2	=	0x2c,
        Ab2	=	0x2c,
        Am2	=	0x2c,

        // MIDI note 45
        // Do Re Mi Fa Sol La Si
        La2	=	0x2d,
        // D E F G A B C
        A2	=	0x2d,

        // MIDI note 46
        // Do Re Mi Fa Sol La Si
        Lad2	=	0x2e,
        LaM2	=	0x2e,
        Sib2	=	0x2e,
        Sim2	=	0x2e,
        // D E F G A B C
        Ad2	=	0x2e,
        AM2	=	0x2e,
        Bb2	=	0x2e,
        Bm2	=	0x2e,

        // MIDI note 47
        // Do Re Mi Fa Sol La Si
        Si2	=	0x2f,
        Dob2	=	0x2f,
        Dom2	=	0x2f,
        // D E F G A B C
        B2	=	0x2f,
        Cb2	=	0x2f,
        Cm2	=	0x2f,

        // MIDI note 48
        // Do Re Mi Fa Sol La Si
        Do3	=	0x30,
        Sid3	=	0x30,
        SiM3	=	0x30,
        // D E F G A B C
        C3	=	0x30,
        Bd3	=	0x30,
        BM3	=	0x30,

        // MIDI note 49
        // Do Re Mi Fa Sol La Si
        Dod3	=	0x31,
        DoM3	=	0x31,
        Reb3	=	0x31,
        Rem3	=	0x31,
        // D E F G A B C
        Cd3	=	0x31,
        CM3	=	0x31,
        Db3	=	0x31,
        Dm3	=	0x31,

        // MIDI note 50
        // Do Re Mi Fa Sol La Si
        Re3	=	0x32,
        // D E F G A B C
        D3	=	0x32,

        // MIDI note 51
        // Do Re Mi Fa Sol La Si
        Red3	=	0x33,
        ReM3	=	0x33,
        Mib3	=	0x33,
        Mim3	=	0x33,
        // D E F G A B C
        Dd3	=	0x33,
        DM3	=	0x33,
        Eb3	=	0x33,
        Em3	=	0x33,

        // MIDI note 52
        // Do Re Mi Fa Sol La Si
        Mi3	=	0x34,
        Fab3	=	0x34,
        Fam3	=	0x34,
        // D E F G A B C
        E3	=	0x34,
        Fb3	=	0x34,
        Fm3	=	0x34,

        // MIDI note 53
        // Do Re Mi Fa Sol La Si
        Fa3	=	0x35,
        Mid3	=	0x35,
        MiM3	=	0x35,
        // D E F G A B C
        F3	=	0x35,
        Ed3	=	0x35,
        EM3	=	0x35,

        // MIDI note 54
        // Do Re Mi Fa Sol La Si
        Fad3	=	0x36,
        FaM3	=	0x36,
        Solb3	=	0x36,
        Solm3	=	0x36,
        // D E F G A B C
        Fd3	=	0x36,
        FM3	=	0x36,
        Gb3	=	0x36,
        Gm3	=	0x36,

        // MIDI note 55
        // Do Re Mi Fa Sol La Si
        Sol3	=	0x37,
        // D E F G A B C
        G3	=	0x37,

        // MIDI note 56
        // Do Re Mi Fa Sol La Si
        Sold3	=	0x38,
        SolM3	=	0x38,
        Lab3	=	0x38,
        Lam3	=	0x38,
        // D E F G A B C
        Gd3	=	0x38,
        GM3	=	0x38,
        Ab3	=	0x38,
        Am3	=	0x38,

        // MIDI note 57
        // Do Re Mi Fa Sol La Si
        La3	=	0x39,
        // D E F G A B C
        A3	=	0x39,

        // MIDI note 58
        // Do Re Mi Fa Sol La Si
        Lad3	=	0x3a,
        LaM3	=	0x3a,
        Sib3	=	0x3a,
        Sim3	=	0x3a,
        // D E F G A B C
        Ad3	=	0x3a,
        AM3	=	0x3a,
        Bb3	=	0x3a,
        Bm3	=	0x3a,

        // MIDI note 59
        // Do Re Mi Fa Sol La Si
        Si3	=	0x3b,
        Dob3	=	0x3b,
        Dom3	=	0x3b,
        // D E F G A B C
        B3	=	0x3b,
        Cb3	=	0x3b,
        Cm3	=	0x3b,

        // MIDI note 60
        // Do Re Mi Fa Sol La Si
        Do4	=	0x3c,
        Sid4	=	0x3c,
        SiM4	=	0x3c,
        // D E F G A B C
        C4	=	0x3c,
        Bd4	=	0x3c,
        BM4	=	0x3c,

        // MIDI note 61
        // Do Re Mi Fa Sol La Si
        Dod4	=	0x3d,
        DoM4	=	0x3d,
        Reb4	=	0x3d,
        Rem4	=	0x3d,
        // D E F G A B C
        Cd4	=	0x3d,
        CM4	=	0x3d,
        Db4	=	0x3d,
        Dm4	=	0x3d,

        // MIDI note 62
        // Do Re Mi Fa Sol La Si
        Re4	=	0x3e,
        // D E F G A B C
        D4	=	0x3e,

        // MIDI note 63
        // Do Re Mi Fa Sol La Si
        Red4	=	0x3f,
        ReM4	=	0x3f,
        Mib4	=	0x3f,
        Mim4	=	0x3f,
        // D E F G A B C
        Dd4	=	0x3f,
        DM4	=	0x3f,
        Eb4	=	0x3f,
        Em4	=	0x3f,

        // MIDI note 64
        // Do Re Mi Fa Sol La Si
        Mi4	=	0x40,
        Fab4	=	0x40,
        Fam4	=	0x40,
        // D E F G A B C
        E4	=	0x40,
        Fb4	=	0x40,
        Fm4	=	0x40,

        // MIDI note 65
        // Do Re Mi Fa Sol La Si
        Fa4	=	0x41,
        Mid4	=	0x41,
        MiM4	=	0x41,
        // D E F G A B C
        F4	=	0x41,
        Ed4	=	0x41,
        EM4	=	0x41,

        // MIDI note 66
        // Do Re Mi Fa Sol La Si
        Fad4	=	0x42,
        FaM4	=	0x42,
        Solb4	=	0x42,
        Solm4	=	0x42,
        // D E F G A B C
        Fd4	=	0x42,
        FM4	=	0x42,
        Gb4	=	0x42,
        Gm4	=	0x42,

        // MIDI note 67
        // Do Re Mi Fa Sol La Si
        Sol4	=	0x43,
        // D E F G A B C
        G4	=	0x43,

        // MIDI note 68
        // Do Re Mi Fa Sol La Si
        Sold4	=	0x44,
        SolM4	=	0x44,
        Lab4	=	0x44,
        Lam4	=	0x44,
        // D E F G A B C
        Gd4	=	0x44,
        GM4	=	0x44,
        Ab4	=	0x44,
        Am4	=	0x44,

        // MIDI note 69
        // Do Re Mi Fa Sol La Si
        La4	=	0x45,
        // D E F G A B C
        A4	=	0x45,

        // MIDI note 70
        // Do Re Mi Fa Sol La Si
        Lad4	=	0x46,
        LaM4	=	0x46,
        Sib4	=	0x46,
        Sim4	=	0x46,
        // D E F G A B C
        Ad4	=	0x46,
        AM4	=	0x46,
        Bb4	=	0x46,
        Bm4	=	0x46,

        // MIDI note 71
        // Do Re Mi Fa Sol La Si
        Si4	=	0x47,
        Dob4	=	0x47,
        Dom4	=	0x47,
        // D E F G A B C
        B4	=	0x47,
        Cb4	=	0x47,
        Cm4	=	0x47,

        // MIDI note 72
        // Do Re Mi Fa Sol La Si
        Do5	=	0x48,
        Sid5	=	0x48,
        SiM5	=	0x48,
        // D E F G A B C
        C5	=	0x48,
        Bd5	=	0x48,
        BM5	=	0x48,

        // MIDI note 73
        // Do Re Mi Fa Sol La Si
        Dod5	=	0x49,
        DoM5	=	0x49,
        Reb5	=	0x49,
        Rem5	=	0x49,
        // D E F G A B C
        Cd5	=	0x49,
        CM5	=	0x49,
        Db5	=	0x49,
        Dm5	=	0x49,

        // MIDI note 74
        // Do Re Mi Fa Sol La Si
        Re5	=	0x4a,
        // D E F G A B C
        D5	=	0x4a,

        // MIDI note 75
        // Do Re Mi Fa Sol La Si
        Red5	=	0x4b,
        ReM5	=	0x4b,
        Mib5	=	0x4b,
        Mim5	=	0x4b,
        // D E F G A B C
        Dd5	=	0x4b,
        DM5	=	0x4b,
        Eb5	=	0x4b,
        Em5	=	0x4b,

        // MIDI note 76
        // Do Re Mi Fa Sol La Si
        Mi5	=	0x4c,
        Fab5	=	0x4c,
        Fam5	=	0x4c,
        // D E F G A B C
        E5	=	0x4c,
        Fb5	=	0x4c,
        Fm5	=	0x4c,

        // MIDI note 77
        // Do Re Mi Fa Sol La Si
        Fa5	=	0x4d,
        Mid5	=	0x4d,
        MiM5	=	0x4d,
        // D E F G A B C
        F5	=	0x4d,
        Ed5	=	0x4d,
        EM5	=	0x4d,

        // MIDI note 78
        // Do Re Mi Fa Sol La Si
        Fad5	=	0x4e,
        FaM5	=	0x4e,
        Solb5	=	0x4e,
        Solm5	=	0x4e,
        // D E F G A B C
        Fd5	=	0x4e,
        FM5	=	0x4e,
        Gb5	=	0x4e,
        Gm5	=	0x4e,

        // MIDI note 79
        // Do Re Mi Fa Sol La Si
        Sol5	=	0x4f,
        // D E F G A B C
        G5	=	0x4f,

        // MIDI note 80
        // Do Re Mi Fa Sol La Si
        Sold5	=	0x50,
        SolM5	=	0x50,
        Lab5	=	0x50,
        Lam5	=	0x50,
        // D E F G A B C
        Gd5	=	0x50,
        GM5	=	0x50,
        Ab5	=	0x50,
        Am5	=	0x50,

        // MIDI note 81
        // Do Re Mi Fa Sol La Si
        La5	=	0x51,
        // D E F G A B C
        A5	=	0x51,

        // MIDI note 82
        // Do Re Mi Fa Sol La Si
        Lad5	=	0x52,
        LaM5	=	0x52,
        Sib5	=	0x52,
        Sim5	=	0x52,
        // D E F G A B C
        Ad5	=	0x52,
        AM5	=	0x52,
        Bb5	=	0x52,
        Bm5	=	0x52,

        // MIDI note 83
        // Do Re Mi Fa Sol La Si
        Si5	=	0x53,
        Dob5	=	0x53,
        Dom5	=	0x53,
        // D E F G A B C
        B5	=	0x53,
        Cb5	=	0x53,
        Cm5	=	0x53,

        // MIDI note 84
        // Do Re Mi Fa Sol La Si
        Do6	=	0x54,
        Sid6	=	0x54,
        SiM6	=	0x54,
        // D E F G A B C
        C6	=	0x54,
        Bd6	=	0x54,
        BM6	=	0x54,

        // MIDI note 85
        // Do Re Mi Fa Sol La Si
        Dod6	=	0x55,
        DoM6	=	0x55,
        Reb6	=	0x55,
        Rem6	=	0x55,
        // D E F G A B C
        Cd6	=	0x55,
        CM6	=	0x55,
        Db6	=	0x55,
        Dm6	=	0x55,

        // MIDI note 86
        // Do Re Mi Fa Sol La Si
        Re6	=	0x56,
        // D E F G A B C
        D6	=	0x56,

        // MIDI note 87
        // Do Re Mi Fa Sol La Si
        Red6	=	0x57,
        ReM6	=	0x57,
        Mib6	=	0x57,
        Mim6	=	0x57,
        // D E F G A B C
        Dd6	=	0x57,
        DM6	=	0x57,
        Eb6	=	0x57,
        Em6	=	0x57,

        // MIDI note 88
        // Do Re Mi Fa Sol La Si
        Mi6	=	0x58,
        Fab6	=	0x58,
        Fam6	=	0x58,
        // D E F G A B C
        E6	=	0x58,
        Fb6	=	0x58,
        Fm6	=	0x58,

        // MIDI note 89
        // Do Re Mi Fa Sol La Si
        Fa6	=	0x59,
        Mid6	=	0x59,
        MiM6	=	0x59,
        // D E F G A B C
        F6	=	0x59,
        Ed6	=	0x59,
        EM6	=	0x59,

        // MIDI note 90
        // Do Re Mi Fa Sol La Si
        Fad6	=	0x5a,
        FaM6	=	0x5a,
        Solb6	=	0x5a,
        Solm6	=	0x5a,
        // D E F G A B C
        Fd6	=	0x5a,
        FM6	=	0x5a,
        Gb6	=	0x5a,
        Gm6	=	0x5a,

        // MIDI note 91
        // Do Re Mi Fa Sol La Si
        Sol6	=	0x5b,
        // D E F G A B C
        G6	=	0x5b,

        // MIDI note 92
        // Do Re Mi Fa Sol La Si
        Sold6	=	0x5c,
        SolM6	=	0x5c,
        Lab6	=	0x5c,
        Lam6	=	0x5c,
        // D E F G A B C
        Gd6	=	0x5c,
        GM6	=	0x5c,
        Ab6	=	0x5c,
        Am6	=	0x5c,

        // MIDI note 93
        // Do Re Mi Fa Sol La Si
        La6	=	0x5d,
        // D E F G A B C
        A6	=	0x5d,

        // MIDI note 94
        // Do Re Mi Fa Sol La Si
        Lad6	=	0x5e,
        LaM6	=	0x5e,
        Sib6	=	0x5e,
        Sim6	=	0x5e,
        // D E F G A B C
        Ad6	=	0x5e,
        AM6	=	0x5e,
        Bb6	=	0x5e,
        Bm6	=	0x5e,

        // MIDI note 95
        // Do Re Mi Fa Sol La Si
        Si6	=	0x5f,
        Dob6	=	0x5f,
        Dom6	=	0x5f,
        // D E F G A B C
        B6	=	0x5f,
        Cb6	=	0x5f,
        Cm6	=	0x5f,

        // MIDI note 96
        // Do Re Mi Fa Sol La Si
        Do7	=	0x60,
        Sid7	=	0x60,
        SiM7	=	0x60,
        // D E F G A B C
        C7	=	0x60,
        Bd7	=	0x60,
        BM7	=	0x60,

        // MIDI note 97
        // Do Re Mi Fa Sol La Si
        Dod7	=	0x61,
        DoM7	=	0x61,
        Reb7	=	0x61,
        Rem7	=	0x61,
        // D E F G A B C
        Cd7	=	0x61,
        CM7	=	0x61,
        Db7	=	0x61,
        Dm7	=	0x61,

        // MIDI note 98
        // Do Re Mi Fa Sol La Si
        Re7	=	0x62,
        // D E F G A B C
        D7	=	0x62,

        // MIDI note 99
        // Do Re Mi Fa Sol La Si
        Red7	=	0x63,
        ReM7	=	0x63,
        Mib7	=	0x63,
        Mim7	=	0x63,
        // D E F G A B C
        Dd7	=	0x63,
        DM7	=	0x63,
        Eb7	=	0x63,
        Em7	=	0x63,

        // MIDI note 100
        // Do Re Mi Fa Sol La Si
        Mi7	=	0x64,
        Fab7	=	0x64,
        Fam7	=	0x64,
        // D E F G A B C
        E7	=	0x64,
        Fb7	=	0x64,
        Fm7	=	0x64,

        // MIDI note 101
        // Do Re Mi Fa Sol La Si
        Fa7	=	0x65,
        Mid7	=	0x65,
        MiM7	=	0x65,
        // D E F G A B C
        F7	=	0x65,
        Ed7	=	0x65,
        EM7	=	0x65,

        // MIDI note 102
        // Do Re Mi Fa Sol La Si
        Fad7	=	0x66,
        FaM7	=	0x66,
        Solb7	=	0x66,
        Solm7	=	0x66,
        // D E F G A B C
        Fd7	=	0x66,
        FM7	=	0x66,
        Gb7	=	0x66,
        Gm7	=	0x66,

        // MIDI note 103
        // Do Re Mi Fa Sol La Si
        Sol7	=	0x67,
        // D E F G A B C
        G7	=	0x67,

        // MIDI note 104
        // Do Re Mi Fa Sol La Si
        Sold7	=	0x68,
        SolM7	=	0x68,
        Lab7	=	0x68,
        Lam7	=	0x68,
        // D E F G A B C
        Gd7	=	0x68,
        GM7	=	0x68,
        Ab7	=	0x68,
        Am7	=	0x68,

        // MIDI note 105
        // Do Re Mi Fa Sol La Si
        La7	=	0x69,
        // D E F G A B C
        A7	=	0x69,

        // MIDI note 106
        // Do Re Mi Fa Sol La Si
        Lad7	=	0x6a,
        LaM7	=	0x6a,
        Sib7	=	0x6a,
        Sim7	=	0x6a,
        // D E F G A B C
        Ad7	=	0x6a,
        AM7	=	0x6a,
        Bb7	=	0x6a,
        Bm7	=	0x6a,

        // MIDI note 107
        // Do Re Mi Fa Sol La Si
        Si7	=	0x6b,
        Dob7	=	0x6b,
        Dom7	=	0x6b,
        // D E F G A B C
        B7	=	0x6b,
        Cb7	=	0x6b,
        Cm7	=	0x6b,

        // MIDI note 108
        // Do Re Mi Fa Sol La Si
        Do8	=	0x6c,
        Sid8	=	0x6c,
        SiM8	=	0x6c,
        // D E F G A B C
        C8	=	0x6c,
        Bd8	=	0x6c,
        BM8	=	0x6c,

        // MIDI note 109
        // Do Re Mi Fa Sol La Si
        Dod8	=	0x6d,
        DoM8	=	0x6d,
        Reb8	=	0x6d,
        Rem8	=	0x6d,
        // D E F G A B C
        Cd8	=	0x6d,
        CM8	=	0x6d,
        Db8	=	0x6d,
        Dm8	=	0x6d,

        // MIDI note 110
        // Do Re Mi Fa Sol La Si
        Re8	=	0x6e,
        // D E F G A B C
        D8	=	0x6e,

        // MIDI note 111
        // Do Re Mi Fa Sol La Si
        Red8	=	0x6f,
        ReM8	=	0x6f,
        Mib8	=	0x6f,
        Mim8	=	0x6f,
        // D E F G A B C
        Dd8	=	0x6f,
        DM8	=	0x6f,
        Eb8	=	0x6f,
        Em8	=	0x6f,

        // MIDI note 112
        // Do Re Mi Fa Sol La Si
        Mi8	=	0x70,
        Fab8	=	0x70,
        Fam8	=	0x70,
        // D E F G A B C
        E8	=	0x70,
        Fb8	=	0x70,
        Fm8	=	0x70,

        // MIDI note 113
        // Do Re Mi Fa Sol La Si
        Fa8	=	0x71,
        Mid8	=	0x71,
        MiM8	=	0x71,
        // D E F G A B C
        F8	=	0x71,
        Ed8	=	0x71,
        EM8	=	0x71,

        // MIDI note 114
        // Do Re Mi Fa Sol La Si
        Fad8	=	0x72,
        FaM8	=	0x72,
        Solb8	=	0x72,
        Solm8	=	0x72,
        // D E F G A B C
        Fd8	=	0x72,
        FM8	=	0x72,
        Gb8	=	0x72,
        Gm8	=	0x72,

        // MIDI note 115
        // Do Re Mi Fa Sol La Si
        Sol8	=	0x73,
        // D E F G A B C
        G8	=	0x73,

        // MIDI note 116
        // Do Re Mi Fa Sol La Si
        Sold8	=	0x74,
        SolM8	=	0x74,
        Lab8	=	0x74,
        Lam8	=	0x74,
        // D E F G A B C
        Gd8	=	0x74,
        GM8	=	0x74,
        Ab8	=	0x74,
        Am8	=	0x74,

        // MIDI note 117
        // Do Re Mi Fa Sol La Si
        La8	=	0x75,
        // D E F G A B C
        A8	=	0x75,

        // MIDI note 118
        // Do Re Mi Fa Sol La Si
        Lad8	=	0x76,
        LaM8	=	0x76,
        Sib8	=	0x76,
        Sim8	=	0x76,
        // D E F G A B C
        Ad8	=	0x76,
        AM8	=	0x76,
        Bb8	=	0x76,
        Bm8	=	0x76,

        // MIDI note 119
        // Do Re Mi Fa Sol La Si
        Si8	=	0x77,
        Dob8	=	0x77,
        Dom8	=	0x77,
        // D E F G A B C
        B8	=	0x77,
        Cb8	=	0x77,
        Cm8	=	0x77,

        // MIDI note 120
        // Do Re Mi Fa Sol La Si
        Do9	=	0x78,
        Sid9	=	0x78,
        SiM9	=	0x78,
        // D E F G A B C
        C9	=	0x78,
        Bd9	=	0x78,
        BM9	=	0x78,

        // MIDI note 121
        // Do Re Mi Fa Sol La Si
        Dod9	=	0x79,
        DoM9	=	0x79,
        Reb9	=	0x79,
        Rem9	=	0x79,
        // D E F G A B C
        Cd9	=	0x79,
        CM9	=	0x79,
        Db9	=	0x79,
        Dm9	=	0x79,

        // MIDI note 122
        // Do Re Mi Fa Sol La Si
        Re9	=	0x7a,
        // D E F G A B C
        D9	=	0x7a,

        // MIDI note 123
        // Do Re Mi Fa Sol La Si
        Red9	=	0x7b,
        ReM9	=	0x7b,
        Mib9	=	0x7b,
        Mim9	=	0x7b,
        // D E F G A B C
        Dd9	=	0x7b,
        DM9	=	0x7b,
        Eb9	=	0x7b,
        Em9	=	0x7b,

        // MIDI note 124
        // Do Re Mi Fa Sol La Si
        Mi9	=	0x7c,
        Fab9	=	0x7c,
        Fam9	=	0x7c,
        // D E F G A B C
        E9	=	0x7c,
        Fb9	=	0x7c,
        Fm9	=	0x7c,

        // MIDI note 125
        // Do Re Mi Fa Sol La Si
        Fa9	=	0x7d,
        Mid9	=	0x7d,
        MiM9	=	0x7d,
        // D E F G A B C
        F9	=	0x7d,
        Ed9	=	0x7d,
        EM9	=	0x7d,

        // MIDI note 126
        // Do Re Mi Fa Sol La Si
        Fad9	=	0x7e,
        FaM9	=	0x7e,
        Solb9	=	0x7e,
        Solm9	=	0x7e,
        // D E F G A B C
        Fd9	=	0x7e,
        FM9	=	0x7e,
        Gb9	=	0x7e,
        Gm9	=	0x7e,

        // MIDI note 127
        // Do Re Mi Fa Sol La Si
        Sol9	=	0x7f,
        // D E F G A B C
        G9	=	0x7f
    };

    QMidiNote(int note);

    int note() const;
    void setNote(int note);

    int number() const;

    int octave() const;

    double frequency() const;

    QString toString() const;
    QString name() const;

    QMidiNote operator=(const QMidiNote& n);

    QMidiNote operator+(const QMidiNote& n);
    QMidiNote operator+=(const QMidiNote& n);
    QMidiNote operator++();
    QMidiNote operator-(const QMidiNote& n);
    QMidiNote operator-=(const QMidiNote& n);
    QMidiNote operator--();

    bool operator<(const QMidiNote& n);
    bool operator<=(const QMidiNote& n);
    bool operator>(const QMidiNote& n);
    bool operator>=(const QMidiNote& n);
    bool operator==(const QMidiNote& n);
    bool operator!=(const QMidiNote& n);

    static QString toString(int note);
    static QMidiNote fromString(QString name);

private:
    int _note;
};

#endif // QMIDINOTE_H
